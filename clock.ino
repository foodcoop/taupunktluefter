
const char zero[]  = "0";
const char minus[] = "-";
const char conlon[] = ":";
String getDateTimeFormatted() {
  clock.getTime();
  String result = "";
  
  result += clock.year+2000;
  result += minus;
  if (clock.month<10) result += zero;
  result += clock.month;
  result += minus;
  if (clock.dayOfMonth<10) result += zero;
  result += clock.dayOfMonth;
  
  result += " ";
  
  if (clock.hour<10) result += zero;
  result += clock.hour;
  result += conlon;
  if (clock.minute<10) result += zero;
  result += clock.minute;
  result += conlon;
  if (clock.second<10) result += zero;
  result += clock.second;

  return result;
}

#if SET_DATE_TIME
void setDateTime(int Y, int M, int D, int h, int m, int s, int day_of_week) {
  clock.fillByYMD(Y,M,D);
  clock.fillByHMS(h,m,s);//15:28 30"
  clock.fillDayOfWeek(day_of_week);//Saturday
  clock.setTime();//write time to the RTC chip}
}
#endif
