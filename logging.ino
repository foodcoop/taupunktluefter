void log(const char* file, String line) {
  File dataFile = SD.open(file, FILE_WRITE);
  if (dataFile) {
    line = getDateTimeFormatted() + "," + line;
    dataFile.println(line); // Write onto SD card
    dataFile.close();       // Close file
    #if USE_SERIAL
    Serial.println(line);
    #endif
  } else {
    //error = true;
    //Serial.println("error "+file);
  }
}

