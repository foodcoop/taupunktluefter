/**
 * Stefan Beyer
 * 
 * Liest zwei Temperatur- und Feuchtigkeitssensoren aus, errechnet jeweils die
 * absolute Luftfeuchtigkeit und die Differenz daraus.
 * Überschreitet die Differenz einen bestimmten Wert wird der Motor-Ausgang
 * auf High gesetzt. Er gleibt dann solange an, bis die differenz auf 0 sinkt.
 * Damit kann dann ein Lüfter angesteuert werden.
 * 
 * Die Mess-Daten werden jede Minute mit Zeitstempel auf die SD-Karte
 * (Dateiname tpl.log) geschrieben.
 * 
 * Das LCD kann 3 verschiedene Anzeigen haben (Umschalten mit Button: solange
 * gedrückt halten, bis das Display geleert wird):
 *  1. Temperatur und absolute Luftfeuchtigkeit für Innen und Außen
 *     I 25.00° 12.66%
 *     A 25.00° 13.89%
 *     
 *  2. Temperatur und relative Luftfeuchtigkeit für Innen und Außen
 *     I 25.00° 55.03%
 *     A 25.00° 56.49%
 *  
 *  3. Differenz der absoluten Luftfeuchtigkeit (diff = Innen% – Außen%)
 *     Minimale und maximale Differenz.
 *     I%-A%=-0.24
 *     MN-3.56 MX4.65
 * 
 * 
 * Setup:
 * 
 * Arduino UNO
 * +SD-Shield (ARD SHD SD V4; Arduino Shield - SD-Karte V4)    zum Daten loggen
 * +Grove Base Shield
 * 
 * - Grove TEMP-HUM Sensor außen an D2        
 * - Grove TEMP-HUM Sensor innen an D3
 * - Grove Button an D5                                        um zwischen verschiedenen Anzeigen zu wechseln
 * - Grove LED an D8                                           Statusanzeige des Lüfters
 * - Grove Relais an D6                                        Steuerung des Lüfters
 * - Grove LCD EGB Backlight an I2C                            Anzeigen von Daten
 * - Grove RTC (Uhr) an I2C                                    Zeitgeber für die Logdaten
 * 
 * (D4 wird von SD verwendet!!)
 * 
 * 
 */
 
#include <rgb_lcd.h>
#include "DHT.h"
#include <SPI.h>
#include <SD.h>
#include "DS1307.h"


#define SET_DATE_TIME 0
#if SET_DATE_TIME
#define SDTY 2018
#define SDTM 6
#define SDTD 27
#define SDTH 13
#define SDTI 14
#define SDTS 0
// Mon = 1 ... Son = 7
#define SDTWD 3
#endif

#define USE_SERIAL 0

#define SCHRANKE_UNTEN 0.5

DS1307 clock;


#define FUNCTION_BUTTON 5
const short functionButtonCount = 3;
short functionButtonState = 2;

#define STATUS_LED 8 // 4 is used by the sd shield
#define MOTOR_STATUS 6
#define POT 0

#define SD_CHIP_SELECT 4


// Sensor:
#define DHT_OUT_PIN 2
#define DHT_IN_PIN 3
#define DHTTYPE DHT11   // DHT 11 

DHT dht_out(DHT_OUT_PIN, DHTTYPE);
DHT dht_in(DHT_IN_PIN, DHTTYPE);




// Display:
const byte degree[8] = {
    0b01110,
    0b01010,
    0b01110,
    0b00000,
    0b00000,
    0b00000,
    0b00000,
    0b00000
};
#define CHAR_DEGREE 0
/*const byte colorR = 255;
const byte colorG = 200;
const byte colorB = 80;*/
rgb_lcd lcd;

unsigned long lastLogged,lastbuttontime;
#define LOG_INTERVAL 60000
#define EXIT_INTERVAL 10000
const char logFile[] = "tpl_log.csv";


float h_in, abs_h_in;
float t_in;

float h_out, abs_h_out;
float t_out;

float abs_h_diff;
float max_abs_h_diff=0, min_abs_h_diff=0;

float diff_schranke = 0;

short lastfunctionButton=-1;
short lastMotor = 0;

bool error = false;
short errorBlink = LOW;

short last_pot_value=-1;

int motor = LOW;


void setup() {
  pinMode(STATUS_LED, OUTPUT);
  pinMode(MOTOR_STATUS, OUTPUT);
  pinMode(FUNCTION_BUTTON, INPUT);

  
  #if USE_SERIAL
  Serial.begin(9600); 
  #endif;
  
  clock.begin();

  #if SET_DATE_TIME
  // MON TUE WED THU FRI SAT SUN
  setDateTime(SDTY, SDTM, SDTD,    SDTH, SDTI, SDTS, SDTWD);
  #endif
  
  //Serial.print("Starte LCD...");
  // set up the LCD's number of columns and rows:
  lcd.begin(16, 2);
  lcd.setRGB(50, 50, 50);
  lcd.createChar(CHAR_DEGREE, degree);
  //lcd.autoscroll();
  lcd.clear();
  ////Serial.println("done");
  //lcd.setRGB(colorR, colorG, colorB);
  //lcd.print("TaupunktLuefter!");
  //lcd.setCursor(0,1);
  //lcd.print(getDateTimeFormatted());


  //Serial.print("Starte Sensoren...");
  dht_out.begin();
  dht_in.begin();
  //Serial.println("done");

  //Serial.print("Starte SD-Card...");
  if (SD.begin(SD_CHIP_SELECT)) {
    //Serial.println("done");
    log(logFile, F("T_in,rH_in,H_in,T_out,rH_out,H_out,Diff,Schranke"));
  } else {
    //Serial.println("failed");
  }

  
  delay(1000);
  lcd.clear();

  lastLogged = LOG_INTERVAL;
  lastbuttontime = 0;
}



void loop() {

  if (error) {
    errorBlink = !errorBlink;
    digitalWrite(STATUS_LED, errorBlink);
    delay(200);
    return;
  }

  // StateButton
  short functionButton = digitalRead(FUNCTION_BUTTON);
  if (functionButton != lastfunctionButton) {
    if (functionButton) {
      lastbuttontime = millis();
      functionButtonState++;
      if (functionButtonState >= functionButtonCount) functionButtonState = 0;
      //Serial.print("State: ");
      //Serial.println(functionButtonState);
      lcd.clear();
    } else {
        if (lastbuttontime>0 && millis() - lastbuttontime > EXIT_INTERVAL) { // exit to error mode
            log(logFile, F("Abbruch."));
            error = true;
            return;
        }
        lastbuttontime=0;
    }
    lastfunctionButton = functionButton;
  }




  
  
  h_in  = dht_in.readHumidity();
  t_in  = dht_in.readTemperature();
  h_out = dht_out.readHumidity();
  t_out = dht_out.readTemperature();

  short s = analogRead(POT);
  if (s!=last_pot_value) {
    // s = 0..1023
    diff_schranke = ((float)s*10)/1023;
    last_pot_value = s;
  }

  abs_h_in  = absoluteHumidity(t_in, h_in);
  abs_h_out = absoluteHumidity(t_out, h_out);
  
  abs_h_diff = abs_h_in - abs_h_out;
  
  //if (abs_h_diff>max_abs_h_diff)max_abs_h_diff=abs_h_diff;
  //if (abs_h_diff<min_abs_h_diff)min_abs_h_diff=abs_h_diff;
  
  
  // außen feuchter als innen abs_h_diff < 0
  // außen trockener als innen abs_h_diff > 0
  // Wenn differenz > schranke, motor erstmal anschalten...
  if (abs_h_diff > diff_schranke) {
    motor = HIGH;
  }
  // Wenn angeglichen oder rel. Feuchte unter 65%: motor abschalten!
  if (abs_h_diff <= SCHRANKE_UNTEN || h_in <= 65) {
    motor = LOW;
  }

  if (lastMotor != motor) {
    
    if (motor) {
      log(logFile, F("Motor AN"));
    } else {
      log(logFile, F("Motor AUS"));
    }
    
    lastMotor = motor;
  }

  if (millis() - lastLogged > LOG_INTERVAL) {
    String result = "";
    char sep = ',';
    
    result += t_in;
    result += sep;
    result += h_in;
    result += sep;
    result += abs_h_in;
    result += sep;
    result += t_out;
    result += sep;
    result += h_out;
    result += sep;
    result += abs_h_out;
    result += sep;
    result += abs_h_diff;
    result += sep;
    result += diff_schranke;

    log(logFile, result); 
    
    lastLogged = millis();
  }

  

  digitalWrite(STATUS_LED, motor);
  digitalWrite(MOTOR_STATUS, motor);





  

  if (functionButtonState<=1) {
    lcd.setCursor(0,0);
    lcd.print(F("I:              "));
    lcd.setCursor(2,0);
    if (isnan(t_in) || isnan(h_in)) {
        lcd.print(F("failed"));
        error = true;
        return;
    }
    lcd.print(t_in);
    lcd.write((unsigned char)CHAR_DEGREE);
    lcd.print(" ");
    if (functionButtonState==1) {
      lcd.print(h_in);
      lcd.print("r");
    } else {
      lcd.print(abs_h_in);
    }
    lcd.print("%");
  
    
    lcd.setCursor(0,1);
    lcd.print(F("A:              "));
    lcd.setCursor(2,1);
    if (isnan(t_out) || isnan(h_out)) {
        lcd.print(F("failed"));
        error = true;
        return;
    }
    lcd.print(t_out);
    lcd.write((unsigned char)CHAR_DEGREE);
    lcd.print(" ");
    if (functionButtonState==1) {
      lcd.print(h_out);
      lcd.print("r");
    } else {
      lcd.print(abs_h_out);
    }
    lcd.print("%");
  } else if (functionButtonState == 2) {
    lcd.setCursor(0,0);
    lcd.print(F("I%-A%="));
    lcd.print(abs_h_diff);
    //lcd.setCursor(15,0);
    //if (motor == HIGH) lcd.write("*");
    //else  lcd.write(" ");
    lcd.setCursor(0,1);
    lcd.print(F("Schranke="));
    lcd.print(diff_schranke);
    
    /*lcd.print(F("MN"));
    lcd.print(min_abs_h_diff);
    lcd.print(F("MX"));
    lcd.print(max_abs_h_diff);
    */
  } else {
    // anderer status
  }

  
  delay(50);
}




