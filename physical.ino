const float mw = 18.016; // kg/kmol (Molekulargewicht des Wasserdampfes)
const float R_ = 8314.3; // J/(kmol*K) (universelle Gaskonstante)
const float a_ = 7.5;
const float b_ = 237.3;

float absoluteHumidity(float T, float r) {
  return pow(10,5) * mw/R_ * (r/100 * (6.1078 * pow(10, ((a_*T)/(b_+T)))))   /   (T + 273.15);
}
/*
float AF(float r,float T) {
  return pow(10,5) * mw/R_ * DD(r,T)/TK(T); 
}
float DD(float r, float T) {
  return r/100 * SDD(T);
}
float TK(float T) {
  return T + 273.15;
}
float SDD(float T) {
  return 6.1078 * pow(10, ((a_*T)/(b_+T)));
}
*/


