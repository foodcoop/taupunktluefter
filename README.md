# Automatische Taupunkt-Lüftung

## Übersicht / Bedienung

Liest zwei Temperatur- und Feuchtigkeitssensoren aus, errechnet jeweils die
absolute Luftfeuchtigkeit und die Differenz daraus.
Überschreitet die Differenz einen bestimmten Wert wird der Motor-Ausgang
auf High gesetzt. Er bleibt dann solange an, bis die Differenz auf 0 sinkt.
Damit kann dann ein Lüfter angesteuert werden.

Die Mess-Daten werden jede Minute mit Zeitstempel auf die SD-Karte
(Dateiname tpl_log.csv) geschrieben.

Das LCD kann 3 verschiedene Anzeigen haben (Umschalten mit Button: solange
gedrückt halten, bis das Display geleert wird):
 1. Temperatur und absolute Luftfeuchtigkeit für Innen und Außen
    ```
    I 25.00° 12.66%
    A 25.00° 13.89%
    ```
 2. Temperatur und relative Luftfeuchtigkeit für Innen und Außen
    ```
    I 25.00° 55.03r%
    A 25.00° 56.49r%
    ```
 
 3. Differenz der absoluten Luftfeuchtigkeit (diff = Innen% – Außen%);
    Minimale und maximale Differenz.
    ```
    I%-A%=-0.24
    MN-3.56 MX4.65
    ```

Um das Programm in den Fehlermodus zu versetzen (um die SD-Karte sicher entnehmen zu können)
muss der Knopf etwa 10 Sekunden lange gedrückt werden; wenn danach die LED blinkt, ist das Gerät im "Fehlermodus" und führt keine Schreiboperationen mehr durch.

## Setup

 - **Arduino UNO**
 - + **SD-Shield** (ARD SHD SD V4; Arduino Shield - SD-Karte V4) zum Daten loggen
 - + **Grove Base Shield**
 - Grove TEMP-HUM *Sensor außen an D2*
 - Grove TEMP-HUM *Sensor innen an D3*
 - Grove Button *an D5,        um zwischen verschiedenen Anzeigen zu wechseln*
 - Grove LED *an D8,          Statusanzeige des Lüfters, Fehleranzeige*
 - Grove Relais *an D6,        Steuerung des Lüfters*
 - Grove LCD EGB Backlight *an I2C,     Anzeigen von Daten*
 - Grove RTC (Uhr) *an I2C,             Zeitgeber für die Logdaten*
 - Grove Potentiometer *an A0,          Einstellen des Schwellenwertes*

(D4 wird von SD verwendet!!)

![Aufbau Plan](https://gitlab.com/foodcoop/taupunktluefter/raw/assets/aufbau.svg?sanitize=true)

## Auswertung der Daten

Für die Auswertung der Daten gibt es ein PHP CLI Skript - mit Sicherheit nicht das beste Tool, aber hatte ich halt gerade zur Hand:

### Grafik erzeugen:

```
php -f auswertung.php file=/pfad/zu/TPL_LOG.CSV > a.png
```

![Beispiel Grafik](https://gitlab.com/foodcoop/taupunktluefter/raw/assets/2018-07-10.png)

### kleine Statistik ausgeben

```
php -f auswertung.php stats=1 file=/pfad/zu/TPL_LOG.CSV > o.txt
```

```
Daten von 09.07.2018 23:34 bis 10.07.2018 15:23
ANZ:   5220
MIN:   -1,510
MAX:   3,240
AVG:   2,244
Motor lief:
 1. 10.07.2018 03:18 - 10.07.2018 14:27; 11,15 Stunden

```
